/* rc4.c
 * This is where the actual rc4 implementation is
 *
 * Requires the following to be included:
 *  assert.h, stdint.h, stdio.h, stdlib.h, and "debug.c".
 *
 */


/* A mod function that works as you'd expect. */
int mod(int x, int m) {
	return (x % m + m) % m;
}

/* Swap two unsigned 8-bit ints */
void uint8_swap(uint8_t* a, uint8_t* b) {
	uint8_t temp = *a;
	*a = *b;
	*b = temp;
}

void genbytes(uint8_t **final, char* key, int bytes) {

	dprintf(1, "Entering '%s'\n", __func__);
	dprintf(2, "Key len: %i\n", (int) strlen(key));
	dprintf(2, "Bytes: %i\n", bytes);

	int discard = 1 << 16; /* Amount of bytes to discard */
	uint8_t i = 0, j = 0; /* unsigned 8-bit ints; free modulo */
	uint8_t S[256] = {0}; /* Our array */
	uint8_t K = 0; /* Output for each round */
	uint8_t *out = calloc(bytes, sizeof(uint8_t)); /* Output */

	/* Key scheduling algorithm */
	dprintf(3, "Doing the KSA\n");
	for (uint8_t k = 0; k < 255; k++) S[k] = k;

	/* Mix in the key */
	dprintf(3, "Mixing in the key\n");
	for (uint8_t k = 0; k < 255; k++) {

		j = mod((j + S[k] + key[mod(k, strlen(key))]), 256);
		/* Note that we use the mod function instead of the %-operator */

		uint8_swap(&S[i], &S[j]);
	}

	/* Reset i and j */
	i = 0;
	j = 0;

	dprintf(3, "Doing the number generation\n");
	for (int k = 0; k < discard+bytes; k++) {

		j += S[++i]; /* two things in one line */

		uint8_swap(&i, &j);

		/* Don't use the discarded bytes */
		if (k >= discard) {

			/* Output our bytes here */
			K = S[mod(S[i] + S[j], 256)];

			/* Have to account for the discarded bytes */
			out[k - discard] = K;
		}

	}

	dprintf(3, "Setting 'final' to the output\n");
	*final = out;
	out = NULL;
	free(out);
	dprintf(1, "Exiting '%s'\n", __func__);

}
