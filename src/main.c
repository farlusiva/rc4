/* main.c
 *
 * I gave a go at implementing rc4 in C.
 * https://en.wikipedia.org/wiki/Arcfour
 *
 */

/* TODO: Key stretching
 * Make password entry hidden like in sudo
 * Allow for 'before' inputs above 4096 chars (see: N_TTY_BUF_SIZE)
 */

#define _POSIX_C_SOURCE 200809L /* Use POSIX.1-2008 */
#define INITIALBUFSIZE 1000 /* Initial buffer size for strings */

#include <assert.h> /* crypto.c, rc4.c */
#include <stdint.h> /* crypto.c, rc4.c */
#include <stdio.h> /* printf */
#include <stdlib.h> /* crypto.c, rc4.c */
#include <string.h> /* rc4.c */
#include <unistd.h> /* getopt */

#include "debug.c"
#include "rc4.c"
#include "crypto.c"


int main(int argc, char* argv[]) {

	dprintf(1, "Entering '%s'\n", __func__);
	dprintf(1, "Debug level: %i\n", DEBUG);
	dprintf(2, "argc: %i\n", argc);
	dprintf(2, "argv: \n");
	for (int k = 0; k < argc; k++)
		dprintf(2, " '%s'\n", argv[k]);

	char usage[INITIALBUFSIZE]; /* Usage string */
	char* infile = NULL; /* Names of in/output files (stdin/stdout if NULL) */
	char* outfile = NULL;
	char key[INITIALBUFSIZE]; /* The key */
	uint8_t *before; /* cipher-/plaintext input goes here */
	uint8_t* after; /* our output after encrypting or decrypting */
	int opt; /* For getopt */
	int crnt_bfsiz = 1; /* Buffer size of 'before' */
	int p = 0; /* Temp int, used in reading 'before' */
	int tmpc; /* Temp int, also used in reading 'before' */
	FILE *in; /* input file to read from */
	FILE *out; /* Output file */

	/* Set up the usage string */
	sprintf(usage, "%s [options]", argv[0]);

	dprintf(3, "Parsing args\n");
	/* parse the options */
	while ((opt = getopt(argc, argv, "hi:o:")) != -1) {

		switch (opt) {

			/* help menu */
			case 'h':
				printf("rc4 program or something\n"
				       "Usage: %s\n"
				       "\n"
				       "Options:\n"
				       "\t-h: Display this menu\n"
				       "\t-i: Take input from file\n"
				       "\t-o: Send output to file\n"
				       , usage);
				return 0;

			/* input from file */
			case 'i':
				infile = optarg;
				break;

			/* output to file */
			case 'o':
				outfile = optarg;
				break;

			/* This should handle the cases when the program expects an
			 * argument, but isn't given one.
			 * We rely in optopt to give the user the correct error message.
			 */
			case '?':
			default:
				return 1;
		}

	}


	/* the key is stdin no matter what */
	/* TODO: Give the key the ability to resize when needed. */

	printf("Enter your key: ");
	fgets(key, sizeof(key), stdin);


	/* Begin reading 'before': */
	dprintf(3, "Reading input file\n");

	p = 0; /* Reset 'p' before use, as it's a temp variable */
	before = calloc(INITIALBUFSIZE, sizeof(uint8_t));
	in = (infile == NULL) ? stdin : fopen(infile, "r");

	if (in == NULL) {
		fprintf(stderr, "Failed to open input file\n");
		return 1;
	}

	/* Get the input */
	if (in == stdin)
		printf("Enter your input: ");


	/* Read the input so we can xor */
	for (;;) {
		tmpc = fgetc(in);

		if (tmpc == EOF) break;

		/* realloc right before when needed */
		if (p >= crnt_bfsiz-1) {
			before = (uint8_t*) realloc(before, ++crnt_bfsiz);

			if (before == NULL) {
				free(before);
				fprintf(stderr, "Could not realloc. Aborting\n");
				return 1;

			}
		}

		before[p++] = tmpc;
		if (tmpc == '\n' && infile == NULL) break;

	}

	if (in != stdin)
		fclose(in);


	dprintf(3, "XORing\n");
	/* encrypt = decrypt because of the xor */
	after = (uint8_t*) calloc(crnt_bfsiz + 1, sizeof(uint8_t));
	rc4_xor(&after, key, before, crnt_bfsiz);

	/* Print the output: */
	dprintf(3, "Writing output\n");
	out = (outfile == NULL) ? stdout : fopen(outfile, "w+");

	if (out == NULL) {
		fprintf(stderr, "Failed to open output file\n");
		return 1; /* bail */
	}

	/* write the output to stdout or file */
	for (int k = 0; k < crnt_bfsiz-1; k++) {

		if (fputc(after[k], out) == EOF) {
			fprintf(stderr, "Failed to write to file\n");
			return 1;
		}

	}

	/* we're closing up; there's no need to worry about closing stdout. */
	fclose(out);

	free(before);
	free(after);

	dprintf(1, "Exiting '%s'\n", __func__);
	return 0;
}
