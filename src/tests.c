/* This is where the tests lie.
 *
 */

#define _POSIX_C_SOURCE 200809L /* Use POSIX.1-2008 */

#include <assert.h> /* crypto.c, rc4.c */
#include <stdint.h> /* crypto.c, rc4.c */
#include <stdio.h> /* crypto.c */
#include <stdlib.h> /* crypto.c, rc4.c */
#include <string.h> /* rc4.c */
#include <time.h> /* srand */

#include "debug.c"
#include "rc4.c"
#include "crypto.c"

void test_encrypt_decrypt() {

	char* key = "abc";
	char* plain_str = "This is the plaintext!";
	uint8_t plain[28];

	for (int k = 0; k < 22; k++)
		plain[k] = plain_str[k];

	uint8_t* b;
	rc4_xor(&b, key, plain, 22);
	rc4_xor(&b, key, b, 22);
	/* encrypt then decrypt */

	for (int k = 0; k < 22; k++)
		assert(b[k] == plain[k]);

	key = "abc";
	plain_str = "Attack during morning hours\n";

	for (int k = 0; k < 28; k++)
		plain[k] = plain_str[k];


	rc4_xor(&b, key, plain, 28);
	rc4_xor(&b, key, b, 28);

	for (int k = 0; k < 28; k++)
		assert(b[k] == plain[k]);


}

int main() {
	assert(mod(5, 2) == 1);
	assert(mod(1, 1) == 0);

	/* Seed the PRNG */
	srand(time(NULL));

	test_encrypt_decrypt();

	for (int k = 0; k < 10000; k++) {

		/* In the block so they can be redeclared at a later point */
		/* up to 9999 char long key and plaintext */
		int keylen = (rand() % 9999) + 1;
		int plainlen = (rand() % 9999) + 1;
		char* key;
		uint8_t* plain;
		uint8_t* out;

		/* calloc with appropriate lengths */
		out = (uint8_t*) calloc(plainlen, sizeof(uint8_t));
		plain = (uint8_t*) calloc(plainlen, sizeof(uint8_t));
		key = (char*) calloc(keylen, sizeof(char));

		/* Generate the random key and plaintext, and ensure that they're all
		 * 8-bit but not a NUL */
		for (int k = 0; k < keylen; k++)
			key[k] = (rand() % 255) + 1;

		for (int k = 0; k < plainlen; k++)
			plain[k] = (rand() % 255) + 1;

		/* Encrypt and decrypt */
		rc4_xor(&out, key, plain, plainlen);
		rc4_xor(&out, key, out, plainlen);

		/* compare byte by byte; strcmp can't be used as they aren't strings */
		for (int k = 0; k < plainlen; k++)
			assert(out[k] == plain[k]);

		/* Since we have used calloc, we free afterwards */
		free(plain);
		free(key);

	}


	return 0;
}
