/* crypto.c
 * This is where the xor i.e. encrypt/decrypt function is.
 *
 * Requires the following to be included:
 *  assert.h, stdint.h, strlib.h, "rc4.c", and "debug.c".
 */

/* Xor (i.e. encrypt/decrypt) function */
void rc4_xor(uint8_t **output, char* key, uint8_t* input, int inlen) {

	dprintf(1, "Entering '%s'\n", __func__);
	dprintf(2, "inlen: %i\n", inlen);

	uint8_t *a; /* the 'random' key to xor with */
	uint8_t* out; /* our output */

	dprintf(3, "Generating bytes\n");
	genbytes(&a, key, inlen);
	out = calloc(inlen + 1, sizeof(uint8_t));
	/* Have to calloc so we can use it as an array.
	 * Note that it's an off by one. It fixes some obscure
	 * bug for some reason.
	 */

	dprintf(3, "XORing\n");
	for (int k = 0; k < inlen; k++)
		out[k] = a[k] ^ input[k]; /* XOR */


	dprintf(3, "Setting 'output' to the output\n");
	*output = out;
	out = NULL;
	free(out);

	dprintf(1, "Exiting '%s'\n", __func__);

}
