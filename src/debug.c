/* debug.c
 *
 * A debug printf macro for when it is needed.
 *
 * See this:
 * stackoverflow.com/questions/1644868/c-define-macro-for-debug-printing/1644898
 * and this: stackoverflow.com/questions/1941307/c-debug-print-macros/27351464
 */

/* Debug level-ometer:
 * 0: No debugging at all
 * 1: Display messages when non-trivial functions are entered and exited
 * 2: Information about arguments to function calls
 * 3: When a function begins on a new particular task
 * 4 (and up): To be defined
  */

#ifndef DEBUG
#define DEBUG 0
#endif /* DEBUG */

/* This macro avoids the problem of always needing an argument for the
 * '...', while also having a leveled system and displaying information
 * about the debug print caller. */

#define dprintf(lvl, ...) \
	do { if (DEBUG >= lvl) { fprintf(stderr, "DEBUG: %s:%d:%s(): ", \
	__FILE__, __LINE__, __func__); fprintf(stderr, __VA_ARGS__);}} while (0)
