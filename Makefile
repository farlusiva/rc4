CC ?= cc
CFLAGS ?= -O2 -Wall -pipe -std=c99 -pedantic
.PHONY: clean


# Allow 'DEBUG' to be set with 'make DEBUG=(num)'
ifdef DEBUG
CFLAGS+=-DDEBUG=$(DEBUG)
endif

all: main

main tests:
	$(CC) $(CFLAGS) src/$@.c -o $@


clean:
	rm -f *.o src/*.o main tests a.out
